import axios from 'axios';
import {BACKEND_URL} from '../environment';

export function signin(data: any) {
  return axios.post(`${BACKEND_URL}/account/signin`, data, {
    headers: {
      contentType: 'application/json',
      accept: 'application/json',
    }
  })
}

export function signup(data: any) {
  return axios.post(`${BACKEND_URL}/account/signup`, data, {
      headers: {
        contentType: 'application/json',
        accept: 'application/json',
      }
    })
}

export function storeUserInfo(data: any) {

  const token = data.token;
  const user = data.user;
  localStorage.setItem('jwt_token', token);
  localStorage.setItem('user', JSON.stringify(user));
  axios.defaults.headers.common['Authorization'] = `JWT ${token}`;
}

export function getUserInfo(){
  return {
    token: localStorage.getItem('jwt_token'),
    user: JSON.parse(localStorage.getItem('user') || '{}'),
  }
}
export function isLoggedIn(){
  return !!getUserInfo().token;
}
const userInfo = getUserInfo();
axios.defaults.headers.common['Authorization'] = `JWT ${userInfo.token}`;