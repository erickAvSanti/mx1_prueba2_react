import process from "process";

const development: boolean = !process.env.NODE_ENV || process.env.NODE_ENV === 'development';

export function isDevMode(): boolean
{
    return development;
}

export const BACKEND_URL = isDevMode() ? 'http://localhost:3001' : 'https://domain.com';