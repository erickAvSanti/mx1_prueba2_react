export function max_length(str: string, max = 20){
  if( str.length > max ) {
    return `${str.substr(0,max)}...`;
  }
  return str;
}