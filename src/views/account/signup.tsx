import React from 'react';
import Button from 'react-bootstrap/esm/Button';
import Form from 'react-bootstrap/esm/Form';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import * as authService from './../../shared/services/auth.service';

class SignUp extends React.Component{
  private emailRef: any = null;
  private passwordRef: any = null;
  private nameRef: any = null;
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
    this.emailRef = React.createRef();
    this.passwordRef = React.createRef();
    this.nameRef = React.createRef();
  }
  onSubmit = (event: any) => {
    event.preventDefault();
    console.log(event);
    
    const email = this.emailRef.current.value;
    const password = this.passwordRef.current.value;
    const name = this.nameRef.current.value;

    authService.signup({name, email, password})
    .then( onSuccess => {
      authService.storeUserInfo(onSuccess.data);
      Swal.fire('Usuario registrado', '', 'success');
    }).catch( onError => {
      const errors = onError.response.data.errors;
      let html = "<ul class='errors-be-list'>";
      errors.forEach((err: any) => {
        if ( /be blank/.test(err) ) { return; }
        html += `<li>${err}</li>`;
      });
      html += "</ul>";
      Swal.fire({
        title: 'Verifique sus datos',
        html,
        icon: 'warning'
      });
    })

  }
  render(){
    return (
      <React.Fragment>
        <div className="create mh100">
        <Form onSubmit={this.onSubmit}>
          <Form.Group controlId="formBasicName">
            <Form.Label>Name</Form.Label>
            <Form.Control type="text" name="name" placeholder="Enter name" ref={this.nameRef} />
          </Form.Group>

          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" name="email" placeholder="Enter email" ref={this.emailRef} />
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" name="password" placeholder="Password" ref={this.passwordRef} />
          </Form.Group>
          <div className="buttons">

            <Button variant="primary" type="submit">
                Sign up
            </Button>
          </div>
          <div>
              <Link to="/auth/signin">Sign in</Link>
          </div>
        </Form>
        </div>
      </React.Fragment>
    );
  }
}
export default SignUp;
