import React from 'react';
import { withRouter, StaticContext } from "react-router";
import { RouteComponentProps } from 'react-router-dom';
import { BehaviorSubject, Observable } from 'rxjs';
import * as authService from './../shared/services/auth.service';
import CreateLink from './links/createlink';
import TableLink from './links/tablelink';
class Dashboard extends React.Component<RouteComponentProps<any, StaticContext, unknown>>{
  state: Readonly<{newRecord: any}>;
  newRecordSubject: BehaviorSubject<any> = new BehaviorSubject<any>({}); 
  newRecord$: Observable<any> = this.newRecordSubject.asObservable();
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
    if(!authService.isLoggedIn()){
      this.props.history.replace('/auth/signin');
    }
    this.state = {newRecord: null}
  }
  linkCreated = (obj: any) => {
    console.log(obj);
    this.newRecordSubject.next(obj);
  }
  render(){
    return (
      <div>
        <div className="dashboard-container">
          <h3>My Links</h3>
          <CreateLink onCreate={this.linkCreated}/>
          <hr />
          <TableLink newRecord={this.newRecord$}/>
        </div>
      </div>
    );
  }
}
export default withRouter(Dashboard);