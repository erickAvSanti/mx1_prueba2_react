import React from "react";

class Loading extends React.Component{
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
  }
  render(){
    return (
      <div className="loading mh100">
        <span>Loading....</span>
      </div>
    );
  }
}
export default Loading