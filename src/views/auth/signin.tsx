import React from 'react';
import Button from 'react-bootstrap/esm/Button';
import Form from 'react-bootstrap/esm/Form';
import { Link, RouteComponentProps } from 'react-router-dom';
import Swal from 'sweetalert2';
import * as authService from './../../shared/services/auth.service';
import { withRouter, StaticContext } from "react-router";

class SignIn extends React.Component<RouteComponentProps<any, StaticContext, unknown>>{
  private emailRef: any = null;
  private passwordRef: any = null;
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
    this.emailRef = React.createRef();
    this.passwordRef = React.createRef();
    console.log(props);
    if(authService.isLoggedIn()){
      this.props.history.replace('/dashboard');
    }
  }
  onSubmit = (event: any) => {
    event.preventDefault();
    console.log(event);
    
    const email = this.emailRef.current.value;
    const password = this.passwordRef.current.value;

    authService.signin({email, password})
    .then( onSuccess => {
      authService.storeUserInfo(onSuccess.data);
      this.props.history.replace('/dashboard');
    }).catch( onError => {
      const errors = onError.response.data.errors;
      let html = "<ul class='errors-be-list'>";
      errors.forEach((err: any) => {
        if ( /be blank/.test(err) ) { return; }
        html += `<li>${err}</li>`;
      });
      html += "</ul>";
      Swal.fire({
        title: 'Verifique sus datos',
        html,
        icon: 'warning'
      });
    })
  }
  render(){
    return (
      <React.Fragment>
        <div className="login mh100">
        <Form onSubmit={this.onSubmit}>
          <Form.Group>
            <Form.Label>Email</Form.Label>
            <Form.Control type="email" placeholder="Enter email" ref={this.emailRef}/>
          </Form.Group>

          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" ref={this.passwordRef}/>
          </Form.Group>
          <div className="buttons">
            <Button variant="primary" type="submit">
              Sign in
            </Button>
          </div>
          <div>
              <Link to="/account/signup">Sign up</Link>
          </div>
        </Form>
        </div>
      </React.Fragment>
    );
  }
}
export default withRouter(SignIn);
