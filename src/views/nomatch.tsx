import React from 'react';
import { withRouter, StaticContext } from "react-router";
import { RouteComponentProps } from 'react-router-dom';
class NoMatch extends React.Component<RouteComponentProps<any, StaticContext, unknown>>{
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
  }
  render(){
    const style = {
      width: '100vw', 
      height: '100vh', 
      display: 'flex', 
      alignItems: 'center', 
      justifyContent: 'center'
    };
    return (
      <div style={style}>
        <div>
          Not found
        </div>
      </div>
    );
  }
}
export default withRouter(NoMatch);