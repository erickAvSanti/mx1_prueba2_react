import axios from 'axios';
import {BACKEND_URL} from '../../shared/environment';

export function shortestlink(data: any) {
  return axios.post(`${BACKEND_URL}/links/shortest`, data, {
    headers: {
      contentType: 'application/json',
      accept: 'application/json',
    }
  })
}

export function all(params: any) {
  return axios.get(`${BACKEND_URL}/links`, {
    headers: {
      contentType: 'application/json',
      accept: 'application/json',
    },
    params
  })
}
export function updateLink(data: { id: any; }, props: {url: string, slug: string}){
  return axios.put(`${BACKEND_URL}/links/${data.id}`, {data, ...props}, {
    headers: {
      contentType: 'application/json',
      accept: 'application/json',
    }
  })
}
export function deleteUrl(data: { id: any; }){
  return axios.delete(`${BACKEND_URL}/links/${data.id}`,{
    headers: {
      contentType: 'application/json',
      accept: 'application/json',
    }
  })
}
export function allStats(data: { id: any; }){
  return axios.get(`${BACKEND_URL}/links/stats/${data.id}`,{
    headers: {
      contentType: 'application/json',
      accept: 'application/json',
    }
  })
}