import React from 'react';
import { withRouter, StaticContext } from "react-router";
import { RouteComponentProps } from 'react-router-dom';
import * as linkService from './link.service';
import TablePagination from '@material-ui/core/TablePagination';
import { Observable, timer } from 'rxjs';
import TableLinkRow from './tablelinkrow';
import SyncIcon from '@material-ui/icons/Sync';
import CircularProgress from '@material-ui/core/CircularProgress';

interface TransferData{
  
  pagination_size: number; 
  page: number;
  total_records: number; 
  total_pages: number;
  records: Array<any>;
  sync: boolean;
  
}

class TableLink extends React.Component<RouteComponentProps<any, StaticContext, unknown> & {newRecord: Observable<any>}>{

  state: TransferData = {
    pagination_size: 20, 
    page: 1, 
    total_records: 1, 
    total_pages: 1,
    records: [],
    sync: false,
  };
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
    this.props.newRecord.subscribe(
      next => {
        timer(1).subscribe( () => this.get());
      }
    )
  }
  componentDidMount(){

    this.get();
  }

  get = () => {
    if( this.state.sync ) { return; }
    this.setState({sync: true});
    linkService.all({
      pagination_size: this.state.pagination_size, 
      page: this.state.page
    }).then(onSuccess => {
      console.log(onSuccess.data);
      this.setState({
        page: onSuccess.data.page, 
        pagination_size: onSuccess.data.pagination_size,
        total_records: onSuccess.data.total_records,
        total_pages: onSuccess.data.total_pages,
        records: onSuccess.data.records,
        sync: false,
      })
    }).catch( onError => {
      console.log(onError);
      this.setState({
        sync: false,
      })
    })
  }

  handleChangePage = (event: any, newPage: number) => {
    this.setState({page: newPage + 1});
    timer(1).subscribe( () => this.get());
  };

  reload = () => {
    timer(1).subscribe( () => this.get());
  }

  handleChangeRowsPerPage = (event: { target: { value: string | number; }; }) => {
    console.log('handleChangeRowsPerPage',event.target.value);
    this.setState({page: 1, pagination_size: +event.target.value});
    timer(1).subscribe( () => this.get());
  };
  onRowUpdate = (event: any) => {
    this.reload();
  }
  onRowDelete = (event: any) => {
    this.reload();
  }
  render(){
    const { records, page, pagination_size, total_records } = this.state;
    return (
      <div className="table-container">
          <table className="table table-borderer table-stripped">
              <thead>
                  <tr>
                      <th>
                        { !this.state.sync && <SyncIcon onClick={this.reload} /> }
                        { this.state.sync && <CircularProgress size={20} /> }
                      </th>
                      <th>URL</th>
                      <th>URL corta</th>
                      <th>Clickeado</th>
                      <th>Creado</th>
                      <th></th>
                  </tr>
              </thead>
              <tbody>
                {
                  records.map( (el, idx) => {
                    return <TableLinkRow 
                      onUpdate={this.onRowUpdate} 
                      onDelete={this.onRowDelete} 
                      page={page} 
                      pagination_size={pagination_size} 
                      el={el} 
                      idx={idx}
                      key={el.id} />
                  })
                }
              </tbody>
          </table>
          <TablePagination
            rowsPerPageOptions={[10, 20, 50, 100]}
            component="div"
            count={total_records}
            rowsPerPage={pagination_size}
            page={page > 0 ? page - 1 : page}
            onChangePage={this.handleChangePage}
            onChangeRowsPerPage={this.handleChangeRowsPerPage}
          />
      </div>
    );
  }
}
export default withRouter(TableLink);