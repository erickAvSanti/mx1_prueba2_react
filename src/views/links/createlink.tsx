import React from 'react';
import { Alert, Form } from 'react-bootstrap';
import { withRouter, StaticContext } from "react-router";
import { RouteComponentProps } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import * as linkService from './link.service';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
class CreateLink extends React.Component<RouteComponentProps<any, StaticContext, unknown> & {onCreate: Function}>{
  
  public state: Readonly<{value: string, sync: boolean, errors: Array<any>}>;
  public props: any;
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
    this.state = {value: '', sync: false, errors: []};
  }
  onSubmit = (event: any) => {
    event.preventDefault();
    console.log(event);
    if( this.state.sync ){ return; }
    console.log(this.state.value);
    this.setState({sync: true});
    linkService.shortestlink({value: this.state.value})
    .then(onSuccess => {
      console.log(onSuccess);
      this.setState({value: '', sync: false, errors: []});
      this.props.onCreate(onSuccess.data);
    })
    .catch(onError => {
      console.log(onError);
      this.setState({sync: false, errors: onError.response.data.errors});
    })
  }
  handleChange = (event: any) => {
    event.preventDefault();
    this.setState({value: event.target.value});

  }
  render(){
    return (
      <Form onSubmit={this.onSubmit}>
        {
          this.state.errors.map( (error, idx) => {
            return <Alert key={idx} variant="danger">{error}</Alert>
          })
        }
        <Form.Group>
          <Form.Label>URL</Form.Label><br />
          <TextField 
            disabled={this.state.sync}
            className="full-width"
            label="Generar URL" 
            variant="outlined" 
            placeholder="ej: https://google.com" 
            value={this.state.value} onChange={this.handleChange} />
          <Form.Text className="text-muted">
            Crear la URL más corta posible
          </Form.Text>
        </Form.Group>
        <Button 
          variant="contained" 
          color="primary" 
          disabled={this.state.sync}
          type="submit">
          Generar { this.state.sync && <CircularProgress size={20} /> }
        </Button>
        
      </Form>
    );
  }
}
export default withRouter(CreateLink);