import { withRouter, StaticContext } from "react-router";
import { DateTime } from "luxon";
import * as utils from './../../shared/utils';
import { BACKEND_URL } from '../../shared/environment';
import DeleteIcon from '@material-ui/icons/Delete';
import { RouteComponentProps } from "react-router-dom";
import React, { Fragment } from "react";
import Button from '@material-ui/core/Button';
import * as linkService from './link.service';
import TextField from '@material-ui/core/TextField';
import Swal from "sweetalert2";
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';
import ShowChartIcon from '@material-ui/icons/ShowChart';
import CloseRoundedIcon from '@material-ui/icons/CloseRounded';
import CircularProgress from '@material-ui/core/CircularProgress';
import SyncIcon from '@material-ui/icons/Sync';
import EditIcon from '@material-ui/icons/Edit';

class TableLinkRow extends React.Component<RouteComponentProps<any, StaticContext, unknown> & {el: any, page: number, pagination_size: number, idx: number, onUpdate: Function, onDelete: Function}>{
  public props: any;
  public state: Readonly<{
    visibleRowPanel: boolean, 
    url: string, 
    slug: string, 
    sync: boolean, 
    stats: Array<any>,
    percentage: number
  }>;
  // eslint-disable-next-line @typescript-eslint/no-useless-constructor
  constructor(props: any){
    super(props);
    this.state = {
      visibleRowPanel: false, 
      url: props.el.url || '', 
      slug: props.el.slug || '', 
      sync: false, 
      stats: [],
      percentage: 0
    };
  }
  togglePanel = () => {
    this.setState({visibleRowPanel: !this.state.visibleRowPanel})
  }
  handleChange = (event: any) => {
    const state: any = {};
    state[event.target.name] = event.target.value;
    this.setState(state);
  }
  close = () => {
    if( this.state.sync ){ return; }
    this.setState({visibleRowPanel: false});
  }
  update = () => {

    if( this.state.sync ){ return; }

    Swal.fire({
      title: '¿Actualizar URL?',
      text: '',
      returnInputValueOnDeny: false,
      icon: 'info',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showCancelButton: true,
      allowOutsideClick: false,
    })
    .then( _ => {
      if ( _.value === true ) {
        if( this.state.sync ){ return; }

        linkService.updateLink(this.props.el, this.state)
        .then(onSuccess => {
          console.log(onSuccess);
          this.setState({sync: false});
          this.props.onUpdate(onSuccess.data);
        })
        .catch(onError => {
          this.setState({sync: false});
          console.log(onError);
        })

      }
    });
  }
  remove = () => {

    if( this.state.sync ){ return; }

    Swal.fire({
      title: '¿Eliminar URL?',
      text: '',
      returnInputValueOnDeny: false,
      icon: 'info',
      confirmButtonText: 'Si',
      cancelButtonText: 'No',
      showCancelButton: true,
      allowOutsideClick: false,
    })
    .then( _ => {
      if ( _.value === true ) {
        if( this.state.sync ){ return; }

        linkService.deleteUrl(this.props.el)
        .then(onSuccess => {
          console.log(onSuccess);
          this.setState({sync: false});
          this.props.onDelete(onSuccess.data);
        })
        .catch(onError => {
          this.setState({sync: false});
          console.log(onError);
        })

      }
    });
  }
  getStats = () => {
    if( this.state.sync ){ return; }
    this.setState({sync: true});
    linkService.allStats(this.props.el)
    .then(onSuccess => {
      console.log(onSuccess);
      this.setState({
        stats: onSuccess.data.records, 
        percentage: onSuccess.data.percentage, 
        sync: false});
    })
    .catch(onError => {
      this.setState({sync: false});
      console.log(onError);
    })
  }
  render(){
    const {el, idx, page, pagination_size} = this.props;
    return (
      <Fragment>
        <tr>
          <td>
            { idx + 1 + (page - 1) * pagination_size }
          </td>
          <td>
            <a href={el.url} target="_blank" rel="noreferrer">
              {utils.max_length(el.url,30)}
            </a>
          </td>
          <td>
            <a href={ `${BACKEND_URL}/s/${el.slug}` } target="_blank" rel="noreferrer">
              {el.slug}
            </a>
          </td>
          <td>{ el.clicked }</td>
          <td>{ DateTime.fromISO(el.created_at).toFormat('dd/MM/yy hh:mm:ss a') }</td>
          <td>
            <div>
              <MoreHorizIcon onClick={this.togglePanel}/>
              <DeleteIcon onClick={this.remove}/>
            </div>
          </td>
        </tr>
        <tr className={'row-panel ' + (this.state.visibleRowPanel ? 'visible' : '')}>
          <td colSpan={Object.keys(el).length}>
            <div className={'element-detail ' + (!this.state.visibleRowPanel ? 'no-height' : '')}>
              <TextField 
                disabled={this.state.sync}
                className="full-width mt-3"
                label="Modificar URL" 
                variant="outlined" 
                name="url"
                value={this.state.url} onChange={this.handleChange} />
              <TextField 
                disabled={this.state.sync}
                className="full-width mt-3"
                label="Modificar fragmento de URL" 
                variant="outlined" 
                name="slug"
                value={this.state.slug} onChange={this.handleChange} />
              <div className="buttons">
                <Button 
                  disabled={this.state.sync}
                  className="mt-2"
                  variant="contained" 
                  color="primary" 
                  onClick={this.update}>
                  Actualizar 
                  { this.state.sync && <CircularProgress size={20} /> }
                  { !this.state.sync && <EditIcon /> }
                </Button>
                <Button 
                  disabled={this.state.sync}
                  className="mt-2 ml-2"
                  variant="contained" 
                  color="secondary" 
                  onClick={this.close}>
                  Cerrar <CloseRoundedIcon />
                </Button>
              </div>
              <div>
                <hr/>
                <ShowChartIcon /> Estadísticas ( Porcentaje de visitas: {(this.state.percentage * 100).toFixed(2)}% )
                <hr/>
                <table className="table table-hover table-borderer table-stripped">
                  <thead>
                    <tr>
                      <th>

                        { this.state.sync && <CircularProgress size={20} /> }
                        { !this.state.sync && <SyncIcon onClick={this.getStats}/> }
                      </th>
                      <th>IP</th>
                      <th>OS</th>
                      <th>User Agent</th>
                      <th>Fecha</th>
                    </tr>
                  </thead>
                  <tbody>
                    {
                      this.state.stats.map( (stat, idx) => {
                        return <tr key={idx}>
                                <td>{ idx + 1}</td>
                                <td>{stat.ip_address}</td>
                                <td>{stat.os}</td>
                                <td>{stat.user_agent}</td>
                                <td>{DateTime.fromISO(stat.created_at).toFormat('dd/MM/yy hh:mm:ss a')}</td>
                              </tr>
                      })
                    }
                  </tbody>
                </table>
              </div>
            </div>
          </td>
        </tr>
      </Fragment>
    );
  }
}

export default withRouter(TableLinkRow);