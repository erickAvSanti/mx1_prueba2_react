
import React, { Suspense } from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";
import './App.css';
import Loading from "./views/loading";



const SignIn = React.lazy(() => import('./views/auth/signin'));
const SignUp = React.lazy(() => import('./views/account/signup'));
const Dashboard = React.lazy(() => import('./views/dashboard'));
const NoMatch = React.lazy(() => import('./views/nomatch'));



function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Redirect
            to={{
              pathname: "/auth/signin",
            }}
          />
        </Route>
        <Route path="/auth/signin">
          
          <Suspense fallback={<Loading />}>
            <SignIn />
          </Suspense>
        </Route>
        <Route path="/account/signup">
          
          <Suspense fallback={<Loading />}>
            <SignUp />
          </Suspense>
        </Route>
        <Route path="/dashboard">
          
          <Suspense fallback={<Loading />}>
            <Dashboard />
          </Suspense>
        </Route>
        <Route path="*">
          <Suspense fallback={<Loading />}>
            <NoMatch />
          </Suspense>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
